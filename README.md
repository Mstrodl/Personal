# SpiderGaming's Website
Welcome to the code for my website!



# Updates
* Added background color to match the system theme
* Fixed the secret code
* Local storage is all part of one item
* Made the lists js have less code


# To Do
* [x] Make all local storage items use on local storage item with a JSON string

* [ ] Use javascript for the user name display

* [ ] Fix the progress bar css

* [ ] Move the secret code to a differant place

* [ ] Add info to the About tab

* [ ] Upload media and add it to the media tab

* [ ] Add settings reset button

* [ ] Add a local storage delete button

* [x] Fix the color set not saving or setting when you reload

* [x] Fix the l and k characters not decoding on the secret code
* There was an error with the ' and ’ (k) and the " and ” (l)

* [x] Use JavaScript to make lists easier to organize 
* I've mostly got this done, just need to add the rest of the variables and copy and paste a lot of things.

* [ ] Fix button padding on the left and right

* [ ] Redesign the input boxes
* Honestly, the default style looks ugly, and my style, was me playing with it, looks terrible.

* [x] Use textarea for secret code inputs
* Done! it looks a little better. There is more design to do, but i will do it later.

* [x] Make the tabs stay on what one was selected when you reload


# File Tree
```
Personal
├── 404.html
├── CHANGELOG
├── Handwriting1.ttf
├── index.html
├── LICENSE.md
├── Pages
│   ├── Javatabs
│   └── syntax.html
├── README.md
├── SpiderGaminIcon.png
└── Style-JS
    ├── List.js
    ├── mystyle.css
    ├── Navbar.js
    ├── Onload.js
    ├── Secretcode.js
    ├── Settings.js
    └── syntax-highlighting.css
```
# Suggestions
Suggestions and help would be awsome!
